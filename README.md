Contact info: jens.schwamborn@uni.lu

Please cite: https://doi.org/10.1101/2021.07.15.452499  

Complete dataset can be found here: 

# Modamio_2021

## Fig 1b: 
Analysis/quantification of total and conformation specific alpha-synuclein via immunostaining. Acquisition in high content imaging microscope OPERETTA (20X high NA). Analysis in Matlab – 2017b. 
## Fig 2ab:
Analysis/quantification of confocal images (60X) for total and phosphorylates (ps129) alpha-synuclein treated with lambda phosphatase and untreated. 
## Fig 2cd and 8a: 
Analysis/quantification of ps129 alpha-synuclein via immunostaining. Acquisition in high content imaging microscope OPERETTA (20X high NA). Analysis in Matlab – 2017b. 
## Fig 3ab: 
Analysis/quantification of TH & TUJ1 (neuronal population) via immunostaining. Acquisition in high content imaging microscope OPERETTA (20X high NA). Analysis in Matlab 
## Fig 3cd and 8b: 
Analysis/quantification of GFAP (astrocytic population) via immunostaining. Acquisition in high content imaging microscope OPERETTA (20X high NA). Analysis in Matlab – 2017b.
## Fig 4abc and 9a: 
Analysis and comparison with online datasets of transcriptomic data. 
## Fig 4cd and 9bcde: 
Diablo analysis of metabolomic and transcriptomic data. 
## Fig 5bc: 
Analysis of MEA output data. 
## Fig 6ef: 
Analysis/quantification of confocal images (60X) for PSD95 and synaptophysin. 
